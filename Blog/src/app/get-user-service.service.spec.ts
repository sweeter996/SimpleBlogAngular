import { TestBed, inject } from '@angular/core/testing';

import { GetUserServiceService } from './get-user-service.service';

describe('GetUserServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetUserServiceService]
    });
  });

  it('should be created', inject([GetUserServiceService], (service: GetUserServiceService) => {
    expect(service).toBeTruthy();
  }));
});
