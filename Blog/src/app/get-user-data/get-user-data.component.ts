import { Component, OnInit } from '@angular/core';
import {GetUserServiceService} from '../get-user-service.service';
import { User } from '../User';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
@Component({
  selector: 'app-get-user-data',
  templateUrl: './get-user-data.component.html',
  styleUrls: ['./get-user-data.component.css']
})
export class GetUserDataComponent implements OnInit {
  userData: User[];
  constructor(private service: GetUserServiceService) { }

  ngOnInit() {
    this.getUser();
  }
  getUser(): void{
    this.service.getData().subscribe((data:User[])=> this.userData = data);
  }
}
