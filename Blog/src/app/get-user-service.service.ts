import { Injectable, ɵConsole } from '@angular/core';
import {User} from '../app/User';
import {Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class GetUserServiceService {

  private URL: string = 'http://5b0cd6f08126c900149974d1.mockapi.io/User';
  constructor(private http: HttpClient) { }
  getData(): Observable<User[]>{
    return this.http.get<User[]>(this.URL).pipe(catchError(this.handleError('getHeroes', [])));
  }
  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error();
      // log to console instead
  
      // TODO: better job of transforming error for user consumption`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
