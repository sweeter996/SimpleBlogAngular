import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { GetUserDataComponent } from './get-user-data/get-user-data.component';
import { FormsModule} from '@angular/forms';
@NgModule({
  declarations: [
    
    AppComponent,
    GetUserDataComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
