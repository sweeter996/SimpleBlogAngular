import {User} from '../app/User';
export const users: User[] = [
    {"id":"1","name":"name 1"},
    {"id":"2","name":"name 2"},
    {"id":"3","name":"name 3"},
    {"id":"4","name":"name 4"},
    {"id":"5","name":"name 5"},
    {"id":"6","name":"name 6"},
    {"id":"7","name":"name 7"},
    {"id":"8","name":"name 8"},
    {"id":"9","name":"name 9"},
    {"id":"10","name":"name 10"},
    {"id":"11","name":"name 11"},
    {"id":"12","name":"name 12"},
    {"id":"13","name":"name 13"},
    {"id":"14","name":"name 14"},
    {"id":"15","name":"name 15"},
    {"id":"16","name":"name 16"},
    {"id":"17","name":"name 17"},
    {"id":"18","name":"name 18"}
];